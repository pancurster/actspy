#!/usr/bin/env perl
use strict;
use warnings;
use POSIX qw(strftime);

my %sec_per_task;

sub today_log_name() {
    my $datestring = strftime "%Y_%m_%d", localtime;
    return $datestring;
}

sub save_activity() {
    my $datestring = today_log_name();
    open(my $fh, '>', $datestring) or die "Can't create new log file '$datestring': $!";
    for my $tname (keys %sec_per_task) {
        print $fh "$tname;$sec_per_task{$tname}\n";
    }
    close $fh;
    print "report saved";
}

sub load_activity() {
    my $log_file = today_log_name();
    open (my $fh, '<', $log_file) or die "Can't open log file '$log_file': $!";
    while (my $line = <$fh>) {
        chomp $line;
        my @entry = split ";", $line;
        $sec_per_task{$entry[0]} = $entry[1];
    }
}

sub termination_handler {
    print "\n";
    for my $tname (keys %sec_per_task) {
        print "[$sec_per_task{$tname}]seconds: $tname";
    }
    save_activity();
    exit 0;
}

$SIG{INT} = \&termination_handler;
$SIG{TERM} = \&termination_handler;

print load_activity() if -f today_log_name();

my $c = 0;
while (1) {
    chomp(my $task = `./c.sh`);
    $sec_per_task{$task} += 1;
    $c += 1;
    if ($c >= 60) {
        save_activity();
        $c = 0;
    }
    print ".";
    sleep 1;
}

